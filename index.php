<?php require "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Array Manipulation Activity</title>
</head>
<body>
	<h1>Activity - Array Manipulation</h1>

	<h3>Solution for printProducts:</h3>
	<ul>
        <?php printProducts($products); ?>  
    </ul>

	<h3>Solution for countProducts:</h3>
    <ul>
    	<?php countProducts($products); ?>
	</ul>

	<h3>Solution for deleteProducts:</h3>
    <ul>
        <?php deleteProducts($products); ?>
    </ul>


</body>
</html>