<?php 
/*
	Array Manipulation
	Activity Instruction:

		1. Create a function called 'createProduct' that accepts a name of the product and price as its parameter and add it to an array called 'products'.
		2. Create a function called 'printProducts' that displays the lists of elements of products array on an unordered list in the index.php.
		3. Create a function called 'countProducts that displays the count of the elements on a products array in the index.php.
		4. Create a function called 'deleteProduct' that deletes an element to a products array.
*/

	$products = [ ];

	// 1.
		function createProduct($name, $price){
			global $products;
	        $products["$name"] = $price;
	    }

	    createProduct("Maltesers", "60");
	    createProduct("Ferrero", "120");
	    createProduct("Twix", "55");
	    createProduct("ChocNut", "1");
	    createProduct("Hersheys", "80");

	// 2.
	    function printProducts($productList){
	        foreach($productList as $name => $price){
	            echo "$name: $price <br/>";
	        }
	    }

	// 3.
    function countProducts($count){
        $arrLength = count($count);
        print_r("Count of elements in the products' array: $arrLength");
    }

    // 4.
    function deleteProducts($delete){
    	// alternative solution:
    	// array_pop($delete); // deletes the last element in the array.

    	$deleted = array_splice($delete,2,1); // deletes twix
    	// first number corresponds to the index offset to be deleted.
    	// second number corresponds to the length to be deleted.

    	print_r("Deleted element: ");

    	foreach($deleted as $name => $price){
		    echo "$name: $price <br/> <br/>";
		} 

		foreach($delete as $name => $price){
		    echo "$name: $price <br/>";
		} 	
    }


?>